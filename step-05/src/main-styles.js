import {html} from '@polymer/lit-element/lit-element.js';

// possible to put variables inside the style :)

export const style = html`
<style>
    .container
    {
      min-height: 100vh;
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
    }
    .title
    {
      font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
      display: block;
      font-weight: 300;
      font-size: 100px;
      color: #35495e;
      letter-spacing: 1px;
    }
    .subtitle
    {
      font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
      font-weight: 300;
      font-size: 42px;
      color: #526488;
      word-spacing: 5px;
      padding-bottom: 15px;
    }
    .links
    {
      padding-top: 15px;
    }

    .button {

      font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
      background-color: white;
      color: #35495e;
      border: 2px solid #35495e;
      border-radius: 4px;
      padding: 10px 32px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-weight: 600;
      font-size: 20px;
    }
 
    .button:hover {
      background-color: #35495e; 
      color: white;
    }

    .field {
      font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
      font-size: 20px;
      width: 80%;
      padding: 12px 20px;
      margin: 8px 0;
      box-sizing: border-box;      
      border: 2px solid #526488;
      border-radius: 4px;
    }

    .field:focus {
      background-color: whitesmoke;
    }

</style>
`

//    # https://www.w3schools.com/css/css3_buttons.asp
