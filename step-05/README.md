# How to start

## Requirements

- NodeJS
- Yarn: Polymer-CLI is easier to install with **yarn**

## Setup

```shell
# first (only once)
yarn global add polymer-cli
# Then
yarn install
```

## If you use VS Code

- install this: [lit-html](https://marketplace.visualstudio.com/items?itemName=bierner.lit-html)

> *Syntax highlighting and IntelliSense for html inside of JavaScript and TypeScript tagged template strings*

## Start

### Start a dev server

```shell
polymer serve
```

### Build for production and serve locally

Build your project and serve the build locally:

```
polymer build
polymer serve build/default
```
