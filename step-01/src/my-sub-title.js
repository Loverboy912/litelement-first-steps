import { LitElement, html } from '@polymer/lit-element';

export class MySubTitle extends LitElement {
  constructor() {
    super()
  }

  render(){
    return html`
      <h2 class="subtitle">
        Training with 🦊 GitLab is fun
      </h2>    
    `
  }
}
customElements.define('my-sub-title', MySubTitle)